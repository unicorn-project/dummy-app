dummy-app
=========


This dummy application solves a __bitcoin-like__ challenge. It consumes cpu resources to generate random bytes for solving the challenge.
The workload is controlled by the parameter **difficulty**. This parameter specifies the number of bytes that must be zero, so that
the challenge is solved.

The Dummy application uses the Spring Boot framework for Java.

A REST API is also available on **/solve?difficulty=3**.

Getting Started
---------------
In the unlikely case you have not already added Catascopia Monitoring to your application, you need to do that first. For detailed step guide follow the steps [here](https://gitlab.com/unicorn-project/uCatascopia-Agent).


The **pom.xml** file of the application contains the dependencies for Spring Boot, Catascopia Monitoring and AspectJ Weaving.

Note for Catascopia Monitoring we will also add the SpringBootProbe dependency.

```xml

<!-- dependency that will enable aspect/annotation weaving -->
<dependency>
  <groupId>org.aspectj</groupId>
  <artifactId>aspectjweaver</artifactId>
  <version>1.9.1</version>
</dependency>

<!-- catascopia library -->
<dependency>
    <groupId>eu.unicornH2020.catascopia</groupId>
    <artifactId>Catascopia-Agent</artifactId>
    <version>0.0.2-SNAPSHOT</version>
</dependency>

<!-- Custom Probes -->

<!-- spring boot probe for catascopia monitoring -->
<dependency>
    <groupId>eu.unicornH2020.catascopia</groupId>
    <artifactId>SpringBootProbe</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- or you can load the custom BlockChainProbe-->
<dependency>
    <groupId>eu.unicornH2020.catascopia</groupId>
    <artifactId>BlockChainProbe</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>


```

Additionally the **pom.xml** file of your application should contain the following plug-ins in your __build__ section configured as follows:

```xml

<build>
  <plugins>

    <!-- aspectj plugin that will perform the annotation source code weaving-->
    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>aspectj-maven-plugin</artifactId>
      <version>1.11</version>
      <configuration>
        <complianceLevel>1.8</complianceLevel>
        <source>1.8</source>
        <target>1.8</target>
        <showWeaveInfo>true</showWeaveInfo>
        <verbose>true</verbose>
        <Xlint>ignore</Xlint>
        <encoding>UTF-8 </encoding>
        <weaveDependencies>
          <weaveDependency>
            <groupId>eu.unicornH2020.catascopia</groupId>
            <artifactId>Catascopia-Agent</artifactId>
          </weaveDependency>
        </weaveDependencies>
        <aspectLibraries>
          <aspectLibrary>
            <groupId>eu.unicornH2020.catascopia</groupId>
            <artifactId>Catascopia-Agent</artifactId>
          </aspectLibrary>
        </aspectLibraries>
      </configuration>
      <executions>
        <execution>
          <goals>
            <goal>compile</goal>
          </goals>
        </execution>
      </executions>
    </plugin>

    <!-- Spring-Boot plugin -->
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
    </plugin>

  </plugins>

</build>



```


Your **DemoApplication** should look like this:
```java
@SpringBootApplication
@Import({CatascopiaMetricFilter.class, CatascopiaMetricProvider.class})
public class DemoApplication {

	public static void main(String[] args) throws Exception {
        SpringApplication.run(DemoApplication.class, args);

        Catascopia agent = new Catascopia();
		agent.startMonitoring();
    }

}
```

or by using annotations, you can enable the Catascopia agent as follows:

```java
@SpringBootApplication
@Import({CatascopiaMetricFilter.class, CatascopiaMetricProvider.class})
public class DemoApplication {

  @UnicornCatascopiaMonitoring
	public static void main(String[] args) throws Exception {
        SpringApplication.run(DemoApplication.class, args);

    }

}
```

It should be noted  that the ```@UnicornCatascopiaMonitoring ``` annotation can be used to annotate **only main() methods**.

### Probes Configuration Properties

For more information on Probe configuration please see [here](https://gitlab.com/unicorn-project/uCatascopia-Agent).
