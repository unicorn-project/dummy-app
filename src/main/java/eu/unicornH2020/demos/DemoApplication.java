package eu.unicornH2020.demos;

import eu.unicornH2020.catascopia.agent.design.annotations.UnicornCatascopiaMonitoring;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import eu.unicornH2020.catascopia.agent.Catascopia;
import eu.unicornH2020.catascopia.probe.CatascopiaMetricFilter;
import eu.unicornH2020.catascopia.probe.CatascopiaMetricProvider;

@SpringBootApplication
@Import({CatascopiaMetricFilter.class, CatascopiaMetricProvider.class})
public class DemoApplication {

    @UnicornCatascopiaMonitoring
	public static void main(String[] args) throws Exception {
        SpringApplication.run(DemoApplication.class, args);

    }

}
