package eu.unicornH2020.demos;

public class MyModel {

    private final long id;
    private final String iterations;
    private final int difficulty;
    private final boolean completed;
    private final String duration;

    public MyModel(long id, String iterations, int difficulty, boolean completed, String duration) {
        this.id = id;
        this.difficulty = difficulty;
        this.iterations = iterations;
        this.completed = completed;
        this.duration = duration;
    }

    public boolean isCompleted() { return completed; }

    public int getDifficulty() {
        return difficulty;
    }

    public long getId() {
        return id;
    }

    public String getDuration() {
        return duration;
    }

    public String getIterations() {
        return iterations;
    }
}