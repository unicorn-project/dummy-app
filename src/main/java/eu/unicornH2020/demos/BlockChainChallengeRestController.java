package eu.unicornH2020.demos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class BlockChainChallengeRestController {

	private static final String template = "%.3f M";
	private static final AtomicLong ids = new AtomicLong();
    private static final AtomicLong running = new AtomicLong();
    private static final AtomicLong completed = new AtomicLong();

    private static final Queue<BlockChainWorker> list = new ConcurrentLinkedQueue<>();


    @Autowired
    private Stats stats;


	@RequestMapping("/solve")
	public MyModel model(@RequestParam(value="difficulty", defaultValue="24") int difficulty) {

	    long id = ids.incrementAndGet();

		BlockChainWorker worker = new BlockChainWorker(difficulty);
        list.add(worker);
		running.incrementAndGet();

        long time = System.currentTimeMillis();
		double iterations = worker.call() / 1000000f;
		double seconds = (System.currentTimeMillis() - time ) / 1000f;
		String duration = String.format("%.3f s",seconds);
		if(worker.getRunning().get()) {
            running.decrementAndGet();
            completed.incrementAndGet();
            stats.update(difficulty,seconds);
        }

        return new MyModel(id,String.format(template, iterations), difficulty, worker.getRunning().get(), duration);
	}


    @RequestMapping("/info")
    public Map<String,Object> info(){
        Map<String,Object> map = new HashMap<>();
        map.put("running",running.get());
        map.put("completed",completed.get());
        return map;
    }

    @RequestMapping("/reset")
	public Map<String,Object> reset(){

	    Map<String,Object> map = new HashMap<>();

	    list.stream().forEach(e-> e.getRunning().set(false));
	    running.set(0);
        completed.set(0);

        map.put("running",running.get());
        map.put("completed",completed.get());

	    return map;
    }
}
