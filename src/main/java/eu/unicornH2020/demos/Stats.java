package eu.unicornH2020.demos;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class Stats {

    public Map<String,List<Double>> values = new HashMap<>();

    public void update(int difficulty, double seconds) {
        String metric = "avg_time_difficulty_"+difficulty;
        List<Double> list = values.getOrDefault(metric,new LinkedList<>());
        list.add(seconds);
        values.putIfAbsent(metric,list);
    }

    public Map<String,Double> collect(){
        Map<String,Double> result =  values.entrySet().stream().collect(
                Collectors.toMap(
                        Map.Entry::getKey,
                        entry->entry.getValue().stream().mapToDouble(e->e).average().getAsDouble()
                ));

        reset();
        return  result;
    }

    public void reset(){
        values.values().clear();
    }

}
