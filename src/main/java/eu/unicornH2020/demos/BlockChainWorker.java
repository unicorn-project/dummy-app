package eu.unicornH2020.demos;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public class BlockChainWorker implements Callable<Long> {
    private final Random random = new Random();
    final int n;
    final byte [] buffer;
    transient long epoch;
    private final int fullBuckets;
    private final int restBits;
    private final byte lastByte;


    private final AtomicBoolean running = new AtomicBoolean(true);

    public BlockChainWorker(int n){
        this.n = n;
        this.buffer = new byte[n];
        fullBuckets = n/8;
        restBits = n%8;
        this.lastByte = (byte) (( 0x01 << restBits)-1);
    }

    public long getEpoch() {
        return epoch;
    }

    public AtomicBoolean getRunning() {
        return running;
    }

    @Override
    public Long call() {

        boolean flag = false;

        while (!flag) {

            flag = true;
            random.nextBytes(buffer);
            //Check full buckets first
            int i = 0;
            for (i = 0; i < fullBuckets; i++) {
                if (buffer[i] != 0x00)
                    flag = false;
            }

            // Check the rest bits
            // First we mask
            if ((buffer[i] & (lastByte)) != 0x00)
                flag = false;

            epoch++;
            if (!running.get())
                return epoch;
        }
        return epoch;
    }
}
