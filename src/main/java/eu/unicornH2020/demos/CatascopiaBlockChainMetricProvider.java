package eu.unicornH2020.demos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class CatascopiaBlockChainMetricProvider {

    @Autowired
	private Stats stats;
	
	public void updateStats(int diff, double time) {
	    stats.update(diff,time);
	}
	

	@RequestMapping(value = "/catascopia/blockchain",
					method = RequestMethod.GET,
					produces = {"application/json", "text/html"}
				   )
	public String metrics(@RequestParam(name = "raw", defaultValue = "true") boolean raw) {

		Map<String, Double> res = stats.collect();

		if(raw) {
		    String result = res.entrySet().stream().map(entry->entry.getKey() +"|"+entry.getValue()).collect(Collectors.joining("\n"));
		    return result;
		}
		else {
			String result = res.entrySet().stream().map(entry->entry.getKey() +"|"+entry.getValue()).collect(Collectors.joining("\n"));
			return result;
		}

    }
}
