
var running = 0;
var completed =0;
var localRunning = 0;

function solve(){

    console.log('Solving...');

    var difficulty = $("#difficulty").val();

    updateInfo();

    localRunning += 1;

    if(running<localRunning)
        running = localRunning;

    updateView();

    $.get("/solve",{difficulty:difficulty},function(data){

        console.log(data);

        completed = data.completed;

        if(completed){
            element = $("<tr> <td>"+data.id +"</td> <td>"+data.difficulty+"</td> <td>"+data.iterations+"</td><td>"+data.duration+"</td></tr>").hide()
        }else{
            element = $("<tr class='danger'> <td>"+data.id +"</td> <td>"+data.difficulty+"</td> <td>"+data.iterations+"</td><td>"+data.duration+"</td></tr>").hide()
        }

        element.fadeIn(300);

        $("#results").prepend(element);

    }).done(function(){
        if(localRunning>0)
            localRunning -= 1;
        if(localRunning>=0)
            running = localRunning;

        updateInfo();
    });

}

function updateInfo(){

    $.get("/info",function(data){

        completed = data.completed;
        if(data.running>localRunning)
            running = localRunning;


    }).done(function(){
        updateView();
    });
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });

    $("#solve-btn").click(solve);
    $("#reset-btn").click(function(){

        $.get("/reset",function(data){

            completed = data.completed;
            running = data.running;
            localRunning = running;

        }).done(function(){

            updateView();
        });

    });

    updateInfo();

});


function updateView(){
    $("#running").text(running);
    $("#completed").text(completed);
}